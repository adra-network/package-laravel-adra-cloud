<?php

use Adranetwork\AdraCloud\Enums\PaymentProvider;

return [
    PaymentProvider::STRIPE->value => 'stripe',
    PaymentProvider::PAYPAL->value => 'paypal',
    PaymentProvider::CASH->value => 'cash',
    PaymentProvider::CHEQUE->value => 'cheque',
    PaymentProvider::DIRECT_DEPOSIT->value => 'dépot direct',
    PaymentProvider::DIRECT_DEBIT->value => 'débit direct',
    'categories' => [
        'digital' => 'digital',
        'physical' => 'physique',
    ]
];
