<?php

use Adranetwork\AdraCloud\Enums\PaymentStatus;

return [
    PaymentStatus::FAILED->value => 'échoué',
    PaymentStatus::SUCCEEDED->value => 'succès',
    PaymentStatus::PROCESSING->value => 'en traitement',
    PaymentStatus::REFUNDED->value => 'remboursé',
    PaymentStatus::UNKNOWN->value => 'inconnu',
    PaymentStatus::REDIRECTING->value => 'redirection',
    PaymentStatus::CANCELLED->value => 'annulé',

];
