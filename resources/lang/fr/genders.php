<?php

use Adranetwork\AdraCloud\Enums\Gender;

return [
    Gender::MALE->value => 'Homme',
    Gender::FEMALE->value => 'Femme',
    Gender::TRANSGENDER->value => 'Transgenre',
    Gender::NON_BINARY->value => 'Non-binaire',
    Gender::REFUSE_TO_ANSWER->value => 'Refuse de répondre',
];
