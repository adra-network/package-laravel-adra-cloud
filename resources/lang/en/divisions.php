<?php

return [
    "afro" =>  'Africa Regional Office',
    "aro" =>  'ASIA Regional Office',
    "sad" =>  'South American Division',
    "spd" =>  'South Pacific Division',
    "iad" =>  'Inter-American Division',
    "mena" =>  'Middle East and North Africa',
    "nad" =>  'North American Division',
    "ero" =>  'Europe Regional Office',
    "ead" =>  'Euro-Asia Division',
];
