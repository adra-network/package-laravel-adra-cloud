<?php

return [
    111 => "Education",
    122 => "Basic Health",
    140 => "Water Supply & Sanitation",
    311 => "Agriculture",
    720 => "Emergency Response",
    740 => "Disaster Prevention & Preparedness",
];
