<?php

use Adranetwork\AdraCloud\Enums\PaymentStatus;

return [
    PaymentStatus::FAILED->value => 'failed',
    PaymentStatus::SUCCEEDED->value => 'succeeded',
    PaymentStatus::PROCESSING->value => 'processing',
    PaymentStatus::REFUNDED->value => 'refunded',
    PaymentStatus::UNKNOWN->value => 'unknown',
    PaymentStatus::REDIRECTING->value => 'redirecting',
    PaymentStatus::CANCELLED->value => 'cancelled'
];
