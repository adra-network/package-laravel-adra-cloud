<?php

use Adranetwork\AdraCloud\Enums\Gender;

return [
    Gender::MALE->value => 'Male',
    Gender::FEMALE->value => 'Female',
    Gender::TRANSGENDER->value => 'Transgender',
    Gender::NON_BINARY->value => 'Non-binary',
    Gender::REFUSE_TO_ANSWER->value => 'Refuse to answer',
];
