<?php

use Adranetwork\AdraCloud\Enums\PaymentProvider;

return [
    PaymentProvider::STRIPE->value => 'stripe',
    PaymentProvider::PAYPAL->value => 'paypal',
    PaymentProvider::CASH->value => 'cash',
    PaymentProvider::CHEQUE->value => 'cheque',
    PaymentProvider::DIRECT_DEPOSIT->value => 'direct deposit',
    PaymentProvider::DIRECT_DEBIT->value => 'direct debit',
    'categories' => [
        'digital' => 'digital',
        'physical' => 'physical',
    ]
];
