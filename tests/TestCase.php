<?php

namespace Adranetwork\AdraCloud\Tests;



use Adranetwork\AdraCloud\AdraCloudServiceProvider;
use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Junges\Kafka\Providers\LaravelKafkaServiceProvider;
use Orchestra\Testbench\Bootstrap\LoadConfiguration;
use Prwnr\Streamer\StreamerProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected $loadEnvironmentVariables = false;
    protected $enablesPackageDiscoveries = true;

    protected function setUp(): void
    {
        parent::setUp();
        config(['adra-cloud.services.authorization.enabled' => true]);

    }

    /**
     * if loadEnvironmentVariables is true then
     * we load the env files and also our adra-cloud-config
     * loadEnvironmentVariables needs to be false when we rely on other laravel config
     *
     * @param $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        if ($this->loadEnvironmentVariables) {
            $path = __DIR__.'/../';
            $app->useEnvironmentPath($path);
            $app->bootstrapWith([LoadEnvironmentVariables::class]);
            $app->setBasePath($path);
            $app->bootstrapWith([LoadConfiguration::class]);
        }

        $app['config']->set('adra-cloud.services.authorization.class_name', '');

    }


    protected function getPackageProviders($app): array
    {
        return [
            AdraCloudServiceProvider::class,
            StreamerProvider::class,
            LaravelKafkaServiceProvider::class,
        ];
    }
    protected function mockAuthorizedResponse() {
        Http::fake([
            '*' => Http::response([
                'result' => true,
            ], 200),
        ]);
    }
    protected function mockUnauthorizedResponse() {
        Http::fake([
            '*' => Http::response([
                'result' => false,
            ], 200),
        ]);
    }

}
