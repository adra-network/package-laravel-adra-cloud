<?php

namespace Adranetwork\AdraCloud\Tests\Feature;



use Adranetwork\AdraCloud\Authorization\Enforcers\OpaEnforcer;
use Adranetwork\AdraCloud\Facades\AuthorizationEnforcer;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Support\Facades\Http;

class AuthorizationEnforcerFacadeTest extends TestCase
{

  /** @test **/
  public function it_loads_opa_enforcer_by_default()
  {
        $this->assertEquals(OpaEnforcer::class, AuthorizationEnforcer::getEnforcerName());
  }

}
