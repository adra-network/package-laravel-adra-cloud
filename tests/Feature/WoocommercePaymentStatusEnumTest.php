<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Enums\PaymentStatus;
use Adranetwork\AdraCloud\Enums\WoocommercePaymentStatus;
use Adranetwork\AdraCloud\Tests\TestCase;

class WoocommercePaymentStatusEnumTest extends TestCase
{
    /** @test **/
    public function it_can_be_matched_to_a_payment_status_enum ()
    {
        $this->assertEquals(PaymentStatus::FAILED, WoocommercePaymentStatus::FAILED->toPaymentStatus());
        $this->assertEquals(PaymentStatus::CANCELLED, WoocommercePaymentStatus::CANCELLED->toPaymentStatus());
        $this->assertEquals(PaymentStatus::SUCCEEDED, WoocommercePaymentStatus::COMPLETED->toPaymentStatus());
        $this->assertEquals(PaymentStatus::SUCCEEDED, WoocommercePaymentStatus::PROCESSING->toPaymentStatus());
        $this->assertEquals(PaymentStatus::REFUNDED, WoocommercePaymentStatus::REFUNDED->toPaymentStatus());
    }
}
