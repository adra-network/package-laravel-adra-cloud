<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Facades\OpaClient;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;

class OpaClientTest extends TestCase
{
  /** @test **/
  public function it_resolves_to_opa_client ()
  {
       $client = $this->app->make(OpaClient::class);
       $this->assertInstanceOf(OpaClient::class, $client);
       $this->assertInstanceOf(\Adranetwork\AdraCloud\Clients\OpaClient::class, $client->getFacadeRoot());
  }


}
