<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource\Consumer;

use Adranetwork\AdraCloud\EventSource\Consumer\LogTriager;
use Adranetwork\AdraCloud\Facades\EventSource;
use Adranetwork\AdraCloud\Tests\Feature\EventSource\TestEvent;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Support\Facades\Log;
use TiMacDonald\Log\LogEntry;
use TiMacDonald\Log\LogFake;

class LogTriaggerTest extends TestCase
{

    /** @test * */
    public function it_logs_event_name()
    {
        LogFake::bind();

        $streamEvent = new TestEvent([
            'id' => 'some-id'
        ]);

        (new LogTriager())->triage($streamEvent);
        Log::assertLogged(
            fn(LogEntry $log) => $log->message === sprintf(LogTriager::FORMAT,
                    LogTriager::RECEIVED_EVENT, $streamEvent->getClassName())
        );
        Log::assertLogged(
            fn(LogEntry $log) => $log->message === sprintf(LogTriager::FORMAT,
                    LogTriager::TIMESTAMP, $streamEvent->getTimestamp())
        );
        Log::assertLogged(
            fn(LogEntry $log) => $log->message === json_encode($streamEvent)
        );

    }

}
