<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource\Consumer;


use Adranetwork\AdraCloud\EventSource\Consumer\KafkaMessageHandler;
use Adranetwork\AdraCloud\EventSource\Consumer\RedisMessageHandler;
use Adranetwork\AdraCloud\EventSource\Consumer\Triager;
use Adranetwork\AdraCloud\Facades\EventSource;
use Adranetwork\AdraCloud\Tests\Feature\EventSource\TestEvent;
use Adranetwork\AdraCloud\Tests\TestCase;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\ConsumedMessage;
use Mockery\MockInterface;
use Prwnr\Streamer\Contracts\Event;
use Prwnr\Streamer\Contracts\MessageReceiver;
use Prwnr\Streamer\EventDispatcher\ReceivedMessage;

class KafkaMessageHandlerTest extends TestCase
{
    protected $loadEnvironmentVariables = false;

    public function setUp(): void
    {
        parent::setUp();
        \Config::set('adra-cloud.event.broker', 'kafka');

    }

    /** @test * */
    public function it_calls_the_triage_class_and_pass_the_deserialized_stream_event()
    {
        Kafka::fake();

        $streamEvent = new TestEvent([
            'id' => 'some-id'
        ]);

        $this->mock(Triager::class, fn(MockInterface $mock ) =>
        $mock->shouldReceive('triage')
            ->withArgs(fn ($arg) =>
                $arg instanceof TestEvent
                && $arg == $streamEvent
            )
        );

        $received =  new ConsumedMessage(
            topicName: 'mark-post-as-published-topic',
            partition: 0,
            headers: [],
            body: [
                'className' => $streamEvent->getClassName(),
                'eventName' => $streamEvent->getEventName(),
                'data' => json_encode($streamEvent)
            ],
            key: null,
            offset: 0,
            timestamp: 0
        );


        (new KafkaMessageHandler())
            ->__execute($received);
    }

}
