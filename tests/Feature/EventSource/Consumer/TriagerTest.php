<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource\Consumer;

use Adranetwork\AdraCloud\EventSource\Consumer\LogTriager;
use Adranetwork\AdraCloud\EventSource\Consumer\Triager;
use Adranetwork\AdraCloud\EventSource\Consumer\TriagerException;
use Adranetwork\AdraCloud\Tests\TestCase;
use ReflectionException;

class TriagerTest extends TestCase
{

    /** @test **/
    public function loads_logging_triager_by_default ()
    {
         $this->assertInstanceOf(LogTriager::class,
             $this->app->make(Triager::class));
    }

    /** @test **/
    public function throws_an_error_if_triager_cannot_be_instanciated ()
    {
        \Config::set('adra-cloud.event.triager', 'invalid');

        $this->expectException(TriagerException::class);
        $this->app->make(Triager::class);
    }

}
