<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource\Consumer;


use Adranetwork\AdraCloud\EventSource\Consumer\EventMessageHandler;
use Adranetwork\AdraCloud\EventSource\Consumer\RedisMessageHandler;
use Adranetwork\AdraCloud\EventSource\Consumer\Triager;
use Adranetwork\AdraCloud\Tests\Feature\EventSource\TestEvent;
use Adranetwork\AdraCloud\Tests\TestCase;
use Mockery\MockInterface;
use Prwnr\Streamer\Contracts\MessageReceiver;
use Prwnr\Streamer\EventDispatcher\ReceivedMessage;

class RedisMessageHandlerTest extends TestCase
{
    protected $loadEnvironmentVariables = true;

    /** @test **/
    public function message_handler_implements_message_receiver_interface ()
    {
      $interfaces = class_implements(RedisMessageHandler::class);
        $this->assertTrue(in_array(MessageReceiver::class,
        $interfaces
        ));

    }

    /** @test **/
    public function message_handler_implements_message_handler_interface ()
    {
      $interfaces = class_implements(RedisMessageHandler::class);
        $this->assertTrue(in_array(EventMessageHandler::class,
        $interfaces
        ));

    }

    /** @test **/
    public function it_calls_the_triage_class_and_pass_the_deserialized_stream_event ()
    {

        $streamEvent = new TestEvent([
            'id' => 'some-id'
        ]);

        $this->mock(Triager::class, fn(MockInterface $mock ) =>
           $mock->shouldReceive('triage')
               ->withArgs(fn ($arg) =>
                    $arg instanceof TestEvent
                       && $arg == $streamEvent
               )
        );
        $received = new ReceivedMessage('1', [
            'data' => json_encode([
                'className' => $streamEvent->getClassName(),
                'eventName' => $streamEvent->getEventName(),
                'data' => json_encode($streamEvent)
            ])
        ]);

        (new RedisMessageHandler())
            ->handle($received);
    }

}
