<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource;


use Adranetwork\AdraCloud\EventSource\Producer\Producer;
use Adranetwork\AdraCloud\EventSource\StreamEvent;
use Adranetwork\AdraCloud\Facades\EventSource;
use Adranetwork\AdraCloud\Tests\TestCase;

class EventSourceTest extends TestCase
{

    /** @test **/
    public function it_returns_a_producer_class ()
    {
         $this->assertInstanceOf(Producer::class, EventSource::producer());
    }

}
