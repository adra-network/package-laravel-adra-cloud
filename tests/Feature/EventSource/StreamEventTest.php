<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource;


use Adranetwork\AdraCloud\EventSource\Event;
use Adranetwork\AdraCloud\EventSource\StreamEvent;
use Adranetwork\AdraCloud\Tests\TestCase;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;

class StreamEventTest extends TestCase
{
    use WithFaker;

    /** @test **/
    public function it_implements_event_interface ()
    {

        $streamEvent = new TestEvent([
            'id' => $uuid = $this->faker->uuid
        ]);
       $this->assertInstanceOf(Event::class, $streamEvent);
    }
    /** @test **/
    public function it_return_class_name ()
    {
        $streamEvent = new TestEvent([
            'id' => $uuid = $this->faker->uuid
        ]);

        $this->assertEquals(TestEvent::class, $streamEvent->getClassName());
    }

    /** @test **/
    public function it_return_event_name ()
    {
        $streamEvent = new TestEvent([
            'id' => $uuid = $this->faker->uuid
        ]);

        $this->assertEquals('test.event', $streamEvent->getEventName());
    }

    /** @test * */
    public function it_has_timestamp()
    {
        $time = now()->subDay();
        Carbon::setTestNow($time);
        $streamEvent = new TestEvent([
            'id' => $uuid = $this->faker->uuid
        ]);

        $this->assertIsString($streamEvent->getTimestamp());
        $this->assertEquals($time->toDateTimeString(), $streamEvent->getTimestamp());
    }




}
