<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource\Producer;


use Adranetwork\AdraCloud\EventSource\Producer\KafkaProducer;
use Adranetwork\AdraCloud\EventSource\Producer\Producer;
use Adranetwork\AdraCloud\EventSource\Producer\RedisProducer;
use Adranetwork\AdraCloud\Facades\EventSource;
use Adranetwork\AdraCloud\Tests\Feature\EventSource\TestEvent;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Foundation\Bootstrap\LoadEnvironmentVariables;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;

class ProducerTest extends TestCase
{
//    protected $loadEnvironmentVariables = true;

    /** @test **/
    public function producer_is_swapped_depending_of_config ()
    {
         \Config::set('adra-cloud.event.broker', 'kafka');
         $this->assertInstanceOf(KafkaProducer::class, EventSource::producer());
    }

    /** @test **/
    public function default_producer_is_redis ()
    {
        $this->assertInstanceOf(RedisProducer::class, EventSource::producer());
    }


}
