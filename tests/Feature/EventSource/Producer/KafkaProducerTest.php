<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource\Producer;


use Adranetwork\AdraCloud\Facades\EventSource;
use Adranetwork\AdraCloud\Tests\Feature\EventSource\TestEvent;
use Adranetwork\AdraCloud\Tests\TestCase;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;

class KafkaProducerTest extends TestCase
{
    protected $loadEnvironmentVariables = true;

    /**
     * @test *
     * live *
     */
    public function live()
    {
        $this->markTestSkipped();
        \Config::set('adra-cloud.event.kafka.username', env('AD_KAFKA_USERNAME'));
        \Config::set('adra-cloud.event.kafka.password', env('AD_KAFKA_PASSWORD'));
        \Config::set('adra-cloud.event.kafka.brokers', env('AD_KAFKA_BROKERS'));
        \Config::set('adra-cloud.event.kafka.compression_type', env('AD_KAFKA_COMPRESSION_TYPE'));

        $event = new TestEvent([
            'id' => 'some-id'
        ]);
        EventSource::producer()
            ->publishOn($topic = 'test_topic')
            ->withMessage($event)
            ->send();
    }

    /** @test * */
    public function it_calls_the_kafka_library()
    {
        $event = new TestEvent([
            'id' => 'some-id'
        ]);
        Kafka::fake();
        EventSource::producer()
            ->publishOn($topic = 'test')
            ->withMessage($event)
            ->send();

        $expectedMessage = Message::create()->withBody( [
            'className' => $event->getClassName(),
            'eventName' => $event->getEventName(),
            'data' => json_encode($event)
        ]);

        Kafka::assertPublishedOn($topic, $expectedMessage);
    }

    /** @test * */
    public function it_published_on_the_default_topic()
    {
        $event = new TestEvent([
            'id' => 'some-id'
        ]);
        config(['adra-cloud.event.kafka.topic' => $topic = 'some-topic']);
        Kafka::fake();
      EventSource::producer()
            ->publishOn()
          ->withMessage($event)

          ->send();

        $expectedMessage = Message::create()->withBody( [
            'className' => $event->getClassName(),
            'eventName' => $event->getEventName(),
            'data' => json_encode($event)
        ]);
        Kafka::assertPublishedOn($topic, $expectedMessage);
    }

    /** @test * */
    public function it_throws_missing_message_exception_if_no_message()
    {
        Kafka::fake();
        $this->expectException(\Exception::class);
        $this->expectErrorMessage('Event could not be dispatched, missing event data');
        EventSource::producer()
            ->send();
        Kafka::assertNothingPublished();
    }

    protected function setUp(): void
    {
        parent::setUp();
        \Config::set('adra-cloud.event.broker', 'kafka');
    }


}
