<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource\Producer;


use Adranetwork\AdraCloud\Facades\EventSource;
use Adranetwork\AdraCloud\Tests\Feature\EventSource\TestEvent;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Prwnr\Streamer\Facades\Streamer;

class RedisProducerTest extends TestCase
{
    use WithFaker;

//    /** @test **/
    public function live_test ()
    {
        $event = new TestEvent([
            'id' => $uuid = $this->faker->uuid
        ]);
        \Config::set('redis.default.port', 6377);
        EventSource::producer()
            ->publishOn('test')
            ->withMessage($event)
            ->send();

    }

    /** @test **/
    public function it_calls_the_streamer_library ()
    {
        $streamer = Streamer::spy();
        $event = new TestEvent([
            'id' => $uuid = $this->faker->uuid
        ]);
        EventSource::producer()
            ->publishOn('test')
            ->withMessage($event)
            ->send();
        $streamer->shouldHaveReceived('emit');
    }

    /** @test **/
    public function it_throws_missing_message_exception_if_no_message ()
    {
        $this->expectException(\Exception::class);
        $this->expectErrorMessage('Event could not be dispatched, missing event data');
        EventSource::producer()
            ->send();
    }

}
