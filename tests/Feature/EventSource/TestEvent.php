<?php

namespace Adranetwork\AdraCloud\Tests\Feature\EventSource;


use Adranetwork\AdraCloud\EventSource\StreamEvent;

class TestEvent extends StreamEvent
{
    public string $id;
    public ?string $firstName;
    public function __construct(
      array $attributes = []
    )
    {
        parent::__construct();
        $this->id = $attributes['id'];
        $this->firstName = $attributes['firstName'] ?? null;
    }



    public function getEventName(): string
    {
        return 'test.event';
    }


    public function jsonSerialize(): mixed
    {
        return [
           'id' => $this->id,
           'firstName' => $this->firstName,
        ];
    }
}
