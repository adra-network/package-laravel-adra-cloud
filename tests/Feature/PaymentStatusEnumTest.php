<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Enums\Contracts\Defaultable;
use Adranetwork\AdraCloud\Enums\Contracts\Labelable;
use Adranetwork\AdraCloud\Enums\PaymentStatus;
use Adranetwork\AdraCloud\Enums\Traits\CountableEnums;
use Adranetwork\AdraCloud\Tests\TestCase;

class PaymentStatusEnumTest extends TestCase
{
    protected array $interfaces = [];
    protected array $traits = [];
    protected function setUp(): void
    {
        parent::setUp();
        $this->traits = class_uses_recursive(PaymentStatus::class);
        $this->interfaces = class_implements(PaymentStatus::class);
    }

    /** @test **/
    public function it_has_count_method ()
    {
         $this->assertTrue(in_array(CountableEnums::class, $this->traits));
    }

    /** @test **/
    public function it_implements_defaultable_interface ()
    {
        $this->assertTrue(in_array(Defaultable::class, $this->interfaces ));

    }

    /** @test **/
    public function it_implements_labelable_interface ()
    {
        $interfaces =   class_implements(PaymentStatus::class);
        $this->assertTrue(in_array(Labelable::class, $interfaces ));
    }

    /** @test **/
    public function label_is_translated ()
    {
        $enum = PaymentStatus::FAILED;
        $english = $enum->label();

        $this->app->setLocale('fr');
        $french = $enum->label();

        $this->assertEquals('failed', $english);
        $this->assertEquals('échoué', $french);
    }

}
