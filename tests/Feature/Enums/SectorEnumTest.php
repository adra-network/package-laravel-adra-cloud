<?php

namespace Adranetwork\AdraCloud\Tests\Feature\Enums;

use Adranetwork\AdraCloud\Enums\Division;
use Adranetwork\AdraCloud\Enums\Sector;
use Adranetwork\AdraCloud\Tests\TestCase;

class SectorEnumTest extends TestCase
{

    /** @test **/
    public function it_has_localized_label ()
    {
         $enum = Sector::Emergency_Response;
         $this->assertEquals(__('adra-cloud::sectors.'.$enum->value), $enum->label());
    }
    /** @test **/
    public function it_can_be_transformed_as_resource ()
    {
        $enum = Sector::Emergency_Response;
        $this->assertEqualsCanonicalizing(
            [
                'label' => $enum->label(),
                'name' => $enum->name,
                'value' => $enum->value,
            ],
            $enum->asResource()
        );
    }
}
