<?php

namespace Adranetwork\AdraCloud\Tests\Feature\Enums;

use Adranetwork\AdraCloud\Enums\Division;
use Adranetwork\AdraCloud\Enums\Gender;
use Adranetwork\AdraCloud\Tests\TestCase;

class DivisionEnumTest extends TestCase
{

    /** @test **/
    public function it_has_localized_label ()
    {
         $enum = Division::Inter_America_Division;
         $this->assertEquals(__('adra-cloud::divisions.'.$enum->value), $enum->label());
    }

    /** @test **/
    public function it_can_be_as_resource ()
    {
         $enum = Division::Inter_America_Division;
         $this->assertEqualsCanonicalizing(
             [
                 'label' => $enum->label(),
                 'name' => $enum->name,
                 'value' => $enum->value,
             ],
             $enum->asResource()
         );
    }



}
