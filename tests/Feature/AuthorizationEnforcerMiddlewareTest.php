<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Authorization\Exceptions\ServerErrorException;
use Adranetwork\AdraCloud\Authorization\Middlewares\AuthorizationEnforcerMiddleware;
use Adranetwork\AdraCloud\Facades\AuthorizationEnforcer;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Routing\Router;

class AuthorizationEnforcerMiddlewareTest extends TestCase
{



    /**
     * Define routes setup.
     *
     * @param  Router  $router
     *
     * @return void
     */
    protected function defineRoutes($router)
    {
        $router->get('/test', function (){
            return response()->json(['some-content']);
        })->middleware(AuthorizationEnforcerMiddleware::class);
    }

    /** @test **/
    public function it_skips_authorization_if_disabled ()
    {
        config(['adra-cloud.services.authorization.enabled' => false]);
        $this->getJson('/test')
            ->assertSuccessful();
    }

    /** @test **/
    public function it_returns_403_if_opa_returns_false ()
    {
        AuthorizationEnforcer::shouldReceive('allow')
            ->andReturn(false);
         $this->mockUnauthorizedResponse();
         $this->getJson('/test')
         ->assertForbidden();
    }

    /** @test **/
    public function it_returns_403_if_error_thrown()
    {

        AuthorizationEnforcer::shouldReceive('allow')
            ->andThrow(ServerErrorException::class,
                'something-wrong');
         $this->getJson('/test')
                ->assertForbidden();
    }



    /** @test **/
    public function it_let_the_request_through_if_opa_authorize ()
    {
        $this->mockAuthorizedResponse();
        $this->getJson('/test')
            ->assertSuccessful();
    }
}
