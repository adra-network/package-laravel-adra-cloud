<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Authorization\AuthorizationEnforcerContract;
use Adranetwork\AdraCloud\Authorization\Exceptions\BadRequestException;
use Adranetwork\AdraCloud\Authorization\Exceptions\InvalidResponseContentException;
use Adranetwork\AdraCloud\Authorization\Exceptions\ServerErrorException;
use Adranetwork\AdraCloud\Authorization\Exceptions\ServiceNotReadyException;
use Adranetwork\AdraCloud\Facades\AuthorizationEnforcer;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class OpaEnforcerTest extends TestCase
{
    public function setup():void
    {
        parent::setUp();
    }

    /** @test **/
    public function it_sends_data_to_opa_service ()
    {
        $this->mockAuthorizedResponse();
        $expectedBody = json_encode([
            'input' => [
                'authorization' => $token = '123',
                'method' => $method = 'POST',
                'path' => $path = 'users',
                'service' => config('app.name'),
            ],
        ]);
        AuthorizationEnforcer::allow(bearerToken: $token, method: $method, path: $path);

        Http::assertSent(function (Request $request) use ($expectedBody) {
            return $request->isJson() &&
                str_contains($request->url(), config('adra-cloud.services.authorization.url')) &&
                str_contains($request->url(), config('adra-cloud.services.authorization.decision_path')) &&
                $request->method() === "POST"
                && $request->body() === $expectedBody;
        });
    }

    /** @test **/
    public function if_nothing_is_passed_it_defaults_to_current_request ()
    {

        $this->mockAuthorizedResponse();
        Config::set('adra-cloud.services.authorization.token', $expectedToken = 'secret');
        $opaEnforcer = $this->app->make(AuthorizationEnforcerContract::class);

        $currentRequest = request();
        $expectedBody = json_encode([
            'input' => [
                'authorization' => $token = '',
                'method' => $method = $currentRequest->getMethod(),
                'path' => $path = $currentRequest->path(),
                'service' => config('app.name'),
            ],
        ]);
        $opaEnforcer->allow();
        Http::assertSent(function (Request $request) use ($expectedBody, $expectedToken) {
            return  $request->isJson() &&
                    $request->method() === "POST" &&
                    isset($request->headers()['Authorization']) &&
                    Arr::get($request->headers(),'Authorization.0') === "Bearer $expectedToken" &&
                    $request->body() === $expectedBody;
        });
    }

    /** @test **/
    public function we_can_pass_other_attributes_to_opa ()
    {

        $this->mockAuthorizedResponse();
        Config::set('adra-cloud.services.authorization.token', $expectedToken = 'secret');
        $opaEnforcer = $this->app->make(AuthorizationEnforcerContract::class);

        $currentRequest = request();
        $extraAttributes = [
            'att1' => 'value1'
        ];
        $expectedBody = json_encode([
            'input' => [
                'authorization' => $token = '',
                'method' => $method = $currentRequest->getMethod(),
                'path' => $path = $currentRequest->path(),
                'service' => config('app.name'),
                'attributes' => $extraAttributes
            ],
        ]);
        $opaEnforcer->withExtraAttributes($extraAttributes)->allow(null, null, null);
        Http::assertSent(function (Request $request) use ($expectedBody, $expectedToken) {
            return  $request->isJson() &&
                $request->method() === "POST" &&
                isset($request->headers()['Authorization']) &&
                Arr::get($request->headers(),'Authorization.0') === "Bearer $expectedToken" &&
                $request->body() === $expectedBody;
        });
    }
    /** @test * */
    public function it_throws_server_error_exception_if_response_status_500()
    {
        $opaEnforcer = $this->app->make(AuthorizationEnforcerContract::class);

        Http::fake([
            '*' => Http::response(null, 500)
        ]);
        $this->expectException(ServerErrorException::class);
        $opaEnforcer->allow('123', '123', '123');
    }

    /** @test * */
    public function it_throws_bad_request_exception_if_response_status_500()
    {
        $opaEnforcer = $this->app->make(AuthorizationEnforcerContract::class);

        Http::fake([
            '*' => Http::response(null, 404)
        ]);
        $this->expectException(BadRequestException::class);
        $opaEnforcer->allow('123', '123', '123');
    }
    /** @test * */
    public function it_throws_service_not_ready_exception_if_response_doesnt_have_result_key()
    {
        $opaEnforcer = $this->app->make(AuthorizationEnforcerContract::class);

        Http::fake([
            '*' => Http::response([
                ''
            ], 200)
        ]);
        $this->expectException(ServiceNotReadyException::class);
        $opaEnforcer->allow('123', '123', '123');
    }

    /** @test * */
    public function it_throws_invalid_response_content_exception_if_result_is_not_boolean()
    {
        $opaEnforcer = $this->app->make(AuthorizationEnforcerContract::class);

        Http::fake([
            '*' => Http::response([
                'result' => 'not-boolean'
            ], 200)
        ]);
        $this->expectException(InvalidResponseContentException::class);
        $opaEnforcer->allow('123', '123', '123');
    }

    /** @test **/
    public function it_returns_boolean ()
    {
        $opaEnforcer = $this->app->make(AuthorizationEnforcerContract::class);

        Http::fake([
            '*' => Http::response([
                'result' => true
            ], 200)
        ]);
        $result = $opaEnforcer->allow('123', '123', '123');
        $this->assertTrue($result);
    }



}
