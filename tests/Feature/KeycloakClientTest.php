<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Clients\KeycloakClient;
use Adranetwork\AdraCloud\Facades\AuthenticationClient;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class KeycloakClientTest extends TestCase
{
    /** @test **/
    public function its_the_default_binding_for_authentication_client()
    {
        $client = $this->app->make('authentication-client');
        $this->assertInstanceOf(KeycloakClient::class, $client);
    }
    /** @test * */
    public function it_can_make_pending_request()
    {
        $pendingRequest = AuthenticationClient::makeRequest();
        $this->assertInstanceOf(PendingRequest::class, $pendingRequest);
    }

    /** @test * */
    public function it_can_make_admin_request()
    {
        $pendingRequest = AuthenticationClient::makeAdminRequest();
        $this->assertInstanceOf(PendingRequest::class, $pendingRequest);
    }
    /** @test * */
    public function it_sets_the_base_url_correctly()
    {

        Http::fake();
        AuthenticationClient::makeRequest()->get('/test');
        Http::assertSent(function (Request $request) {
//            dd($request->url());
            return $request->url() === config('adra-cloud.services.authentication.url'). config('adra-cloud.services.authentication.auth_prefix') . '/test' ;
        });
    }
    /** @test * */
    public function it_sets_the_base_url_correctly_for_admin_request()
    {
        $path = '/users';
        $expectedUrl = sprintf('%s%s/admin/realms/%s%s',
            config('adra-cloud.services.authentication.url'),
            config('adra-cloud.services.authentication.auth_prefix'),
            config('adra-cloud.services.authentication.realm'),
            $path
        );
        Http::fake();
        AuthenticationClient::makeAdminRequest()->get($path);
        Http::assertSent(function (Request $request) use ($expectedUrl) {
            return $request->url() === $expectedUrl;
        });
    }

    /** @test * */
    public function it_can_generate_authorization_bearer_token_for_client()
    {
        Config::set('adra-cloud.services.authentication.client_id', $clientId = 'some-id');
        Config::set('adra-cloud.services.authentication.client_secret', $clientSecret = 'some-secret');


        Http::fake([
           config('adra-cloud.services.authentication.openid_configuration') =>
                Http::response([
                    "token_endpoint" => $endpoint = "*/token-path",
                ]),
            $endpoint =>
                Http::response([
                    "access_token" => $accessToken = '123',
                ]),
        ]);

        $this->assertEquals($accessToken, AuthenticationClient::generateAccessToken());
        Http::assertSentInOrder([
            function (Request $request) {
                return $request->method() === 'GET' && Str::contains($request->url(), 'realms');
            },
            function (Request $request) use ($clientId, $clientSecret) {
                return
                    $request->isForm() &&
                    $request->method() === 'POST' &&
                    $request->body() === sprintf('grant_type=client_credentials&client_id=%s&client_secret=%s',
                        $clientId, $clientSecret);

            }
        ]);
    }

    /** @test * */
    public function it_can_generate_authorization_bearer_token_for_specific_client()
    {
        Config::set('adra-cloud.services.authentication.client_id', $clientId = 'some-id');
        Config::set('adra-cloud.services.authentication.client_secret', $clientSecret = 'some-secret');
        $overrideClientId = 'override-id';
        $overrideClientSecret = 'override-secret';
        Http::fake([
            config('adra-cloud.services.authentication.openid_configuration') =>
                Http::response([
                    "token_endpoint" => $endpoint = "*/token-path",
                ]),
            $endpoint =>
                Http::response([
                    "access_token" => $accessToken = '123',
                ]),
        ]);

        $this->assertEquals($accessToken, AuthenticationClient::generateAccessToken(
            clientId: $overrideClientId,
            clientSecret: $overrideClientSecret,
        ));
        Http::assertSentInOrder([
            function (Request $request) {
                return $request->method() === 'GET';
            },
            function (Request $request) use ($overrideClientId, $overrideClientSecret) {
                return
                    $request->isForm() &&
                    $request->method() === 'POST' &&
                    $request->body() === sprintf('grant_type=client_credentials&client_id=%s&client_secret=%s',
                        $overrideClientId, $overrideClientSecret);

            }
        ]);
    }

    /** @test **/
    public function cache_is_being_used_agressively ()
    {
        Cache::shouldReceive('remember')->twice()
            ->andReturnValues([
            ['token_endpoint' => 'some-url'],
            'access_token' => $token = 'some-token'
        ]);
        $this->assertEquals($token, AuthenticationClient::generateAccessToken());
    }

    /** @test **/
    public function it_can_reset_user_password()
    {
        $path = '/users';
        $expectedUrl = sprintf('%s%s/admin/realms/%s%s',
            config('adra-cloud.services.authentication.url'),
            config('adra-cloud.services.authentication.auth_prefix'),
            config('adra-cloud.services.authentication.realm'),
            $path
        );

        Http::fakeSequence()
            ->push([
                'token_endpoint' => 'test'
            ], 200)
            ->push([
                'access_token' => '123'
            ])
            ->pushStatus(204)
            ->whenEmpty(Http::response());


        AuthenticationClient::resetUserPassword('username.bob',$password = 'new_password');

        [$resetRequest] = Http::recorded()->last();
         $this->assertStringContainsString($password,$resetRequest->body());
         $this->assertStringContainsString($expectedUrl, $resetRequest->url());
    }

    /** @test **/
    public function it_encodes_username_when_searching_for_id()
    {

        $expectedString = 'john.doe%2Bsecret%40example.com';
        $path = '/users';
        $expectedUrl = sprintf('%s%s/admin/realms/%s%s',
            config('adra-cloud.services.authentication.url'),
            config('adra-cloud.services.authentication.auth_prefix'),
            config('adra-cloud.services.authentication.realm'),
            $path
        );

        Http::fakeSequence()
            ->push([
                'token_endpoint' => 'test'
            ], 200)
            ->push([
                'access_token' => '123'
            ])
            ->push([
               ['id' => '123']
            ])
            ->whenEmpty(Http::response());


        AuthenticationClient::getUserIdByUsername('john.doe+secret@example.com');

        [$getUsernameIdRequest] = Http::recorded()->last();

        $this->assertStringContainsString($expectedString, $getUsernameIdRequest->url());

    }
}
