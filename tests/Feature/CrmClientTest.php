<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Facades\AuthenticationClient;
use Adranetwork\AdraCloud\Facades\CrmClient;
use Adranetwork\AdraCloud\Tests\TestCase;
use Http;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Str;

class CrmClientTest extends TestCase
{
    use WithFaker;
    public function setUp(): void
    {
        parent::setUp();
        AuthenticationClient::shouldReceive('generateAccessToken')->andReturn('123');

    }

    /** @test * */
    public function it_can_make_pending_request()
    {
        $this->assertInstanceOf(PendingRequest::class, CrmClient::makeRequest());
    }

    /** @test **/
    public function it_can_scope_url_to_organization_tenant ()
    {
        \Config::set('adra-cloud.services.crm.url', 'http://crm.adra.test');

        Http::fake();
         $client = CrmClient::makeRequest($tenant = $this->faker->uuid);

         $client->get('/example');

         Http::assertSent(function (Request $request) use ($tenant){
             return $request->hasHeader(config('adra-cloud.tenancy.header_key_identifier'), [
                 $tenant
             ]);
         });
    }

    /** @test **/
    public function base_url_is_crm_base_url_without_tenant ()
    {
        \Config::set('adra-cloud.services.crm.url', $baseUrl = 'http://crm.adra.test');

        Http::fake();
        $client = CrmClient::makeRequest();

        $client->get('/example');

        Http::assertSent(function (Request $request) use ($baseUrl){
            return Str::contains($request->url(), [$baseUrl]);
        });
    }

    /** @test **/
    public function request_sent_contains_bearer_token ()
    {
        Http::fake();
        CrmClient::makeRequest()->get('/example');
        Http::assertSent(fn (Request $request) =>
             $request->hasHeader('Authorization', [
                'Bearer 123'
                ])
        );
    }



}
