<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Logger\ApiLoggerMiddleware;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Routing\Router;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use TiMacDonald\Log\LogEntry;
use TiMacDonald\Log\LogFake;

class ApiLoggerMiddlewareTest extends TestCase
{
    use WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        LogFake::bind();
    }

    /**
     * Define routes setup.
     *
     * @param  Router  $router
     *
     * @return void
     */
    protected function defineRoutes($router)
    {
        $router->get('/test', function (){
            return response()->json(['some-content']);
        })->middleware(ApiLoggerMiddleware::class);
    }

    /** @test * */
    public function it_logs_incoming_request()
    {
        $route = '/test';
        $this->getJson($route, [
            'Authorization' => $bearer = $this->faker->uuid
        ])
            ->assertSuccessful();

        Log::assertLogged(fn (LogEntry $log) =>
            $log->level === 'info'
            && Str::containsAll($log->message, [
                'request_received',
                'GET',
                $bearer,
            ])
        );

    }

    /** @test * */
    public function it_logs_outgoing_response()
    {

        $route = '/test';
        $this->getJson($route, [
            'Authorization' => $bearer = $this->faker->uuid
        ])
            ->assertSuccessful();


        Log::assertLogged(function (LogEntry $log) use ($route) {
            return
                $log->level === 'info' &&
                Str::containsAll($log->message, [
                'response_sent',
                'some-content'
            ]);
        });

    }


    /** @test * */
    public function it_logs_same_request_id_for_request_log_and_response_log()
    {


        $route = '/test';
        $this->getJson($route, [
            'Authorization' => $bearer = $this->faker->uuid
        ])
            ->assertSuccessful();

        $requestIds = [];
        Log::assertLogged(function (LogEntry $log)   use ($route, &$requestIds) {
            $messagesArray = json_decode($log->message, true);
            $requestIds[] = $messagesArray['request_id'];
            return true;
        });

        $this->assertEquals(Arr::first($requestIds), Arr::last($requestIds));
    }


}
