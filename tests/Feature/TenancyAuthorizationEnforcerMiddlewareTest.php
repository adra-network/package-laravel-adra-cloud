<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Authorization\Middlewares\TenancyAuthorizationEnforcerMiddleware;
use Adranetwork\AdraCloud\Tests\TestCase;
use Illuminate\Http\Client\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Arr;
use Mockery\MockInterface;

class TenancyAuthorizationEnforcerMiddlewareTest extends TestCase
{
    protected $loadEnvironmentVariables = false;
    /**
     * Define routes setup.
     *
     * @param  Router  $router
     *
     * @return void
     */
    protected function defineRoutes($router)
    {
        $router->get('/test', function (){
            return response()->json(['some-content']);
        })->middleware(TenancyAuthorizationEnforcerMiddleware::class);
    }

    /** @test **/
    public function it_received_tenancy_as_extra_attributes ()
    {
        \Http::fake([
            '*' => \Http::response(null, 500)
        ]);

        $this->getJson('/test')
            ->assertForbidden();

        \Http::assertSent(function (Request $request){
            $body = json_decode($request->body(), true);
            return \Arr::has($body, 'input.attributes.organization_id') &&
                tenant('id') === Arr::get($body, 'input.attributes.organization_id');
        });
    }

    /** @test **/
    public function it_returns_403_if_opa_returns_false ()
    {
         $this->mockUnauthorizedResponse();
         $this->getJson('/test')
         ->assertForbidden();
    }

    /** @test **/
    public function it_let_the_request_through_if_opa_authorize ()
    {
        $this->mockAuthorizedResponse();
        $this->getJson('/test')
            ->assertSuccessful();
    }
}
