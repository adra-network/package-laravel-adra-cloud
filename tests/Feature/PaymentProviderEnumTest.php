<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Enums\Contracts\Defaultable;
use Adranetwork\AdraCloud\Enums\Contracts\DefaultEnum;
use Adranetwork\AdraCloud\Enums\Contracts\Labelable;
use Adranetwork\AdraCloud\Enums\PaymentProvider;
use Adranetwork\AdraCloud\Enums\Traits\CountableEnums;
use Adranetwork\AdraCloud\Tests\TestCase;

class PaymentProviderEnumTest extends TestCase
{
    protected array $interfaces = [];
    protected array $traits = [];
    protected function setUp(): void
    {
        parent::setUp();
        $this->traits = class_uses_recursive(PaymentProvider::class);
        $this->interfaces = class_implements(PaymentProvider::class);
    }

    /** @test **/
    public function it_has_count_method ()
    {
         $this->assertTrue(in_array(CountableEnums::class, $this->traits));
    }

    /** @test **/
    public function it_implements_defaultable_interface ()
    {
        $this->assertTrue(in_array(Defaultable::class, $this->interfaces ));

    }

    /** @test **/
    public function it_implements_labelable_interface ()
    {
        $interfaces =   class_implements(PaymentProvider::class);
        $this->assertTrue(in_array(Labelable::class, $interfaces ));
    }

    /** @test **/
    public function label_is_translated ()
    {
        $enum = PaymentProvider::DIRECT_DEPOSIT;
        $english = $enum->label();

        $this->app->setLocale('fr');
        $french = $enum->label();

        $this->assertEquals('direct deposit', $english);
        $this->assertEquals('dépot direct', $french);
    }

    /** @test **/
    public function it_has_a_category ()
    {
        $enum = PaymentProvider::CHEQUE;
        $this->assertEquals('physical', $enum->category());
        $this->app->setLocale('fr');
        $this->assertEquals('physique', $enum->category());

    }
    /** @test **/
    public function it_can_be_derived_from_partial_string ()
    {
         $this->assertEquals(PaymentProvider::STRIPE, PaymentProvider::tryFromPartial('sepa_stripe'));
         $this->assertEquals(PaymentProvider::PAYPAL, PaymentProvider::tryFromPartial('something_paypal'));
    }
    /** @test **/
    public function partial_string_throw_error_if_none_find ()
    {
        $this->expectException(\Exception::class);
         $this->assertEquals(PaymentProvider::STRIPE, PaymentProvider::tryFromPartial(''));
    }
}
