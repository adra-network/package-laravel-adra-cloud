<?php

namespace Adranetwork\AdraCloud\Tests\Feature;

use Adranetwork\AdraCloud\Enums\Gender;
use Adranetwork\AdraCloud\Tests\TestCase;

class GenderEnumTest extends TestCase
{

    /** @test **/
    public function it_has_label ()
    {
         $gender = Gender::MALE;
         $this->assertEquals(__('adra-cloud::genders.male'), $gender->label());
    }

    /** @test **/
    public function label_is_localized ()
    {
        $this->app->setLocale('fr');

        $gender = Gender::MALE;
        $this->assertEquals('Homme', $gender->label());
    }

}
