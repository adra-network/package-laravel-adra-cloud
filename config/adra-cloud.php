<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    'event' => [
//       Triager class that implements the Triager Interface
        'triager' => \Adranetwork\AdraCloud\EventSource\Consumer\LogTriager::class,
        'broker' => env('AD_EVENT_BROKER', 'redis'),
        'kafka' => [
            'username' => env('AD_KAFKA_USERNAME', ''),
            'password' => env('AD_KAFKA_PASSWORD', ''),
            'brokers' => env('AD_KAFKA_BROKERS', ''),
            'topic' => env('AD_KAFKA_TOPIC', ''),
            'sasl_mechanisms' => env('AD_KAFKA_SASL_MECHANISM', 'SCRAM-SHA-256'),
            'sasl_security_protocol' => env('AD_KAFKA_SASL_SECURITY_PROTOCOL', 'SASL_SSL'),
            'compression_type' => env('KAFKA_COMPRESSION_TYPE', env('AD_KAFKA_COMPRESSION_TYPE', 'gzip'))
        ],
        'redis' => [
            'stream' => env('AD_REDIS_STREAM', '*'),
        ],
    ],
    'tenancy' => [
        'header_key_identifier' => env('AD_TENANCY_HEADER_KEY_IDENTIFIER', 'X-Organization')
    ],
    'frontends' => [
        'dpr' => [
            'url' => env('AD_FRONTEND_DPR_EXTERNAL_BASE_URL', ''),
        ],
        'dpm' => [
            'url' => env('AD_FRONTEND_DPM_EXTERNAL_BASE_URL', ''),
        ],
        'om' => [
            'url' => env('AD_FRONTEND_OM_EXTERNAL_BASE_URL', ''),
        ],
        'drm' => [
            'url' => env('AD_FRONTEND_DRM_EXTERNAL_BASE_URL', ''),
        ],
    ],
    'services' => [
        'crm' => [
            'url' => env('AD_CRM_BASE_URL', ''),
            'bucket' => env('AD_CRM_BUCKET_NAME'),
        ],
        'uso' => [
            'url' => env('AD_USO_BASE_URL', ''),
            'bucket' => env('AD_USO_BUCKET_NAME'),
        ],
        'campaign' => [
            'url' => env('AD_CAMPAIGN_BASE_URL', ''),
            'bucket' => env('AD_CAMPAIGN_BUCKET_NAME'),

        ],
        'payment' => [
            'url' => env('AD_PAYMENT_BASE_URL', ''),
            'bucket' => env('AD_PAYMENT_BUCKET_NAME'),
        ],
        'asset' => [
            'url' => env('AD_ASSET_BASE_URL',''),
            'bucket' => env('AD_ASSET_BUCKET_NAME'),
        ],
        'authentication' => [
            'url' => env('AD_AUTHENTICATION_BASE_URL', 'https://auth.adra.dev'),
            'auth_prefix' => env('AD_AUTHENTICATION_AUTH_PREFIX', '/auth'),
            'realm' => env('AD_AUTHENTICATION_DEFAULT_REALM', 'network'),
            'openid_configuration' => env('AD_AUTHENTICATION_OPENID_CONFIGURATION', '/.well-known/openid-configuration'),
            'client_id' => env('AD_AUTHENTICATION_CLIENT_ID'),
            'client_secret' => env('AD_AUTHENTICATION_CLIENT_SECRET'),
        ],
        'authorization' => [
            'url' => env('AD_AUTHORIZATION_BASE_URL', ''),
            'bucket' => env('AD_AUTHORIZATION_BUCKET_NAME'),
            'class_name' => env('AD_AUTHORIZATION_AUTHORIZER_CLASS_NAME'),
            'enabled' => (bool) (env('AD_AUTHORIZATION_ENABLED', true)),
            'decision_path' => env('AD_AUTHORIZATION_DECISION_PATH', ''),
            'token' => env('AD_AUTHORIZATION_TOKEN'),
            'timeout' => intval(env('AD_AUTHORIZATION_TIMEOUT', 10)),
            'retry' => intval(env('AD_AUTHORIZATION_RETRY', 1)),
            'retry_sleep' => intval(env('AD_AUTHORIZATION_RETRY_SLEEP', 1)),
        ],
    ],
];
