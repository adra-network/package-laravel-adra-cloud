# ADRA Cloud 

[![Latest Version on Packagist](https://img.shields.io/packagist/v/adranetwork/adra-cloud.svg?style=flat-square)](https://packagist.org/packages/adranetwork/adra-cloud)
[![Total Downloads](https://img.shields.io/packagist/dt/adranetwork/adra-cloud.svg?style=flat-square)](https://packagist.org/packages/adranetwork/adra-cloud)

This package consolidate configurations keys and helpers for our applications
## Installation

You can install the package via composer:

```bash
composer require adranetwork/adra-cloud
```



### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email francois.auclair.911@gmail.com instead of using the issue tracker.

## Credits

-   [Francois Auclair](https://github.com/adranetwork)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
