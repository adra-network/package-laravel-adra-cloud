<?php

namespace Adranetwork\AdraCloud\Enums;

enum Gender: string
{
    case MALE = 'male';
    case FEMALE = 'female';
    case TRANSGENDER = 'transgender';
    case NON_BINARY = 'non-binary';
    case REFUSE_TO_ANSWER = 'refuse-to-answer';


    public function label(): string
    {
        return match($this)
        {
            Gender::MALE => __('adra-cloud::genders.male'),
            Gender::FEMALE => __('adra-cloud::genders.female'),
            Gender::TRANSGENDER => __('adra-cloud::genders.transgender'),
            Gender::NON_BINARY => __('adra-cloud::genders.non-binary'),
            Gender::REFUSE_TO_ANSWER => __('adra-cloud::genders.refuse-to-answer'),
        };
    }

}
