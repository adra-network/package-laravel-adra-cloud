<?php

namespace Adranetwork\AdraCloud\Enums;

use Adranetwork\AdraCloud\Enums\Contracts\Labelable;

enum Division: string  implements Labelable
{
    case Africa_Regional_Office = 'afro';
    case Asia_Regional_Office = 'aro';
    case South_American_Division = 'sad';
    case South_Pacific_Division = 'spd';
    case Inter_America_Division = 'iad';
    case Middle_East_And_North_Africa = 'mena';
    case North_American_Division = 'nad';
    case Europe_Regional_Office = 'ero';
    case Euro_Asia_Division = 'ead';

    public function label(): string
    {
        return  __("adra-cloud::divisions.{$this->value}");
    }
    public function asResource(): array
    {
        return  [
            'label' => $this->label(),
            'name' => $this->name,
            'value' => $this->value
        ];
    }

}
