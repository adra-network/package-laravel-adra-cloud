<?php

namespace Adranetwork\AdraCloud\Enums;

use Adranetwork\AdraCloud\Enums\Contracts\Labelable;

enum Sector: int  implements Labelable
{
    case Education = 111;
    case Basic_Health = 122;
    case Water_Supply_Sanitation = 140;
    case Agriculture = 311;
    case Emergency_Response = 720;
    case Disaster_Prevention_Preparedness = 740;

    public function label(): string
    {
        return  __("adra-cloud::sectors.{$this->value}");
    }

    public function asResource(): array
    {
        return  [
            'label' => $this->label(),
            'name' => $this->name,
            'value' => $this->value
        ];
    }
}
