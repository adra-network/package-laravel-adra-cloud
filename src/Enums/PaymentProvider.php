<?php

namespace Adranetwork\AdraCloud\Enums;

use Adranetwork\AdraCloud\Enums\Contracts\Defaultable;
use Adranetwork\AdraCloud\Enums\Contracts\Labelable;
use Adranetwork\AdraCloud\Enums\Traits\CountableEnums;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

enum PaymentProvider: string implements Defaultable, Labelable
{
    use CountableEnums;

    case STRIPE = 'stripe';
    case PAYPAL = 'paypal';
    case CASH = 'cash';
    case CHEQUE = 'cheque';
    case DIRECT_DEPOSIT = 'direct_deposit';
    case DIRECT_DEBIT = 'direct_debit';


    public function label(): string
    {
        return match($this)
        {
            PaymentProvider::STRIPE => __('adra-cloud::payment-providers.stripe'),
            PaymentProvider::PAYPAL => __('adra-cloud::payment-providers.paypal'),
            PaymentProvider::CASH => __('adra-cloud::payment-providers.cash'),
            PaymentProvider::CHEQUE => __('adra-cloud::payment-providers.cheque'),
            PaymentProvider::DIRECT_DEPOSIT => __("adra-cloud::payment-providers.direct_deposit"),
            PaymentProvider::DIRECT_DEBIT => __('adra-cloud::payment-providers.direct_debit'),
        };
    }
    public function category(): string
    {
        return match($this)
        {
            PaymentProvider::STRIPE => __('adra-cloud::payment-providers.categories.digital'),
            PaymentProvider::PAYPAL => __('adra-cloud::payment-providers.categories.digital'),
            PaymentProvider::CASH => __('adra-cloud::payment-providers.categories.physical'),
            PaymentProvider::CHEQUE => __('adra-cloud::payment-providers.categories.physical'),
            PaymentProvider::DIRECT_DEPOSIT => __('adra-cloud::payment-providers.categories.physical'),
            PaymentProvider::DIRECT_DEBIT => __('adra-cloud::payment-providers.categories.physical'),
        };
    }
    public static function default()
    {
        return PaymentProvider::STRIPE;
    }

    public static function tryFromPartial(string $name): self
    {
        $providers =  array_filter(PaymentProvider::cases(), fn (PaymentProvider $provider) =>
             Str::contains($name, $provider->value, true)
        );
        if (empty($providers)) {
            throw new \Exception('Could not find a match for string '. $name);
        }
        return Arr::first($providers);

    }
}
