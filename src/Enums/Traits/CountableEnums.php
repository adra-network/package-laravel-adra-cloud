<?php

namespace Adranetwork\AdraCloud\Enums\Traits;

trait CountableEnums
{
    public static function count(): int
    {
        return count(self::cases());
    }

}
