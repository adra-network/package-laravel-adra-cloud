<?php

namespace Adranetwork\AdraCloud\Enums;


enum WoocommercePaymentStatus: string
{

    case PENDING = 'pending';
    case PROCESSING = 'processing';
    case ON_HOLD = 'on-hold';
    case COMPLETED = 'completed';
    case CANCELLED = 'cancelled';
    case REFUNDED = 'refunded';
    case FAILED = 'failed';

    public function toPaymentStatus(): PaymentStatus
    {
        return match($this)
        {
            WoocommercePaymentStatus::PENDING,
            WoocommercePaymentStatus::ON_HOLD       => PaymentStatus::PROCESSING,
            WoocommercePaymentStatus::PROCESSING,
            WoocommercePaymentStatus::COMPLETED     => PaymentStatus::SUCCEEDED,

            WoocommercePaymentStatus::CANCELLED     => PaymentStatus::CANCELLED,
            WoocommercePaymentStatus::REFUNDED      => PaymentStatus::REFUNDED,
            WoocommercePaymentStatus::FAILED        => PaymentStatus::FAILED,
        };
    }


}
