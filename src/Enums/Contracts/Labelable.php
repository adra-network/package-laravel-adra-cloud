<?php

namespace Adranetwork\AdraCloud\Enums\Contracts;

interface Labelable
{
    public function label(): string;
}

