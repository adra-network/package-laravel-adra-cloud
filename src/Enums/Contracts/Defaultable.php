<?php

namespace Adranetwork\AdraCloud\Enums\Contracts;

interface Defaultable
{
    public static function default();
}

