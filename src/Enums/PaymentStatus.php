<?php

namespace Adranetwork\AdraCloud\Enums;

use Adranetwork\AdraCloud\Enums\Contracts\Defaultable;
use Adranetwork\AdraCloud\Enums\Contracts\Labelable;
use Adranetwork\AdraCloud\Enums\Traits\CountableEnums;

enum PaymentStatus: string implements Defaultable, Labelable
{
    use CountableEnums;

    case FAILED = 'failed';
    case SUCCEEDED = 'succeeded';
    case PROCESSING = 'processing';
    case REFUNDED = 'refunded';
    case CANCELLED = 'cancelled';
    case REDIRECTING = 'redirecting';
    case UNKNOWN = 'unknown';


    public function label(): string
    {
        return match($this)
        {
            PaymentStatus::FAILED => __('adra-cloud::payment-status.failed'),
            PaymentStatus::CANCELLED => __('adra-cloud::payment-status.cancelled'),
            PaymentStatus::REDIRECTING => __('adra-cloud::payment-status.redirecting'),
            PaymentStatus::SUCCEEDED => __('adra-cloud::payment-status.succeeded'),
            PaymentStatus::PROCESSING => __('adra-cloud::payment-status.processing'),
            PaymentStatus::REFUNDED => __('adra-cloud::payment-status.refunded'),
            PaymentStatus::UNKNOWN => __('adra-cloud::payment-status.unknown'),
        };
    }

    public static function default()
    {
        return PaymentStatus::SUCCEEDED;
    }
}
