<?php

namespace Adranetwork\AdraCloud\Logger;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class ApiLoggerMiddleware
{

    public function __construct(protected string $requestId = '')
    {
        $this->setRequestId(\Str::uuid()->toString());
    }


    /**
     * Handle an incoming request.
     * Logs important detail to logger
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
    }
    /**
     * @param  Request  $request
     * @return string
     */
    protected function prepareRequestLog (Request $request): string
    {
        return json_encode([
            'event_type' => 'request_received',
            'request_id' => $this->requestId ?? '',
            'gdpr_valid' => false,
            'headers' => $request->headers->all(),
            'bearer' => $request->bearerToken(),
            'user_agent' => $request->userAgent(),
            'method' => $request->method(),
            'path' => $request->path(),
            'fullUrl' => $request->fullUrl(),
            'service_requested' => config('app.name', '')
        ]);
    }
    /**
     * @param  Response  $response
     * @return string
     */
    protected function prepareResponseLog(Response $response): string
    {

        return json_encode([
            'event_type' => 'response_sent',
            'request_id' => $this->requestId ?? '',
            'gdpr_valid' => false,
            'headers' => $response->headers->all(),
            'status' => $response->getStatusCode(),
            'content' => $response->getContent(),
        ]);
    }

    /**
     * @param  string  $requestId
     */
    public function setRequestId(string $requestId): void
    {
        $this->requestId = $requestId;
    }

    public function terminate(Request $request, Response $response)
    {
        Log::info($this->prepareRequestLog($request));

        Log::info($this->prepareResponseLog($response));

        $this->setRequestId('');
    }


}
