<?php

namespace Adranetwork\AdraCloud\CurrencyHelper;


use PrinsFrank\Standards\Currency\ISO4217_Alpha_3;

class CurrencyHelper
{
    protected array $NON_DECIMAL_CURRENCIES = [
        ISO4217_Alpha_3::Burundi_Franc,
        ISO4217_Alpha_3::CFA_Franc_BEAC,
        ISO4217_Alpha_3::CFA_Franc_BCEAO,
        ISO4217_Alpha_3::CFP_Franc,
        ISO4217_Alpha_3::Chilean_Peso,
        ISO4217_Alpha_3::Comorian_Franc,
        ISO4217_Alpha_3::Djibouti_Franc,
        ISO4217_Alpha_3::Dong,
        ISO4217_Alpha_3::Guarani,
        ISO4217_Alpha_3::Guinean_Franc,
        ISO4217_Alpha_3::Malagasy_Ariary,
        ISO4217_Alpha_3::Rwanda_Franc,
        ISO4217_Alpha_3::Uganda_Shilling,
        ISO4217_Alpha_3::Vatu,
        ISO4217_Alpha_3::Won,
        ISO4217_Alpha_3::Yen
    ];

    public function __construct(
        protected ?ISO4217_Alpha_3 $currency = null,
        protected ?int    $amount = null
    )
    {}

    protected function setCurrency(ISO4217_Alpha_3|string $currency = null)
    {
        if ($currency instanceof ISO4217_Alpha_3) {
            return  $this->currency = $currency;
        }
        $this->currency = !is_null($currency) ? ISO4217_Alpha_3::tryFrom(strtoupper($currency)) : $this->currency;
    }

    public function of($currency, ?int $amount = null): self
    {
        $this->setCurrency($currency);
        $this->amount = $amount;
        return $this;
    }

    public function ofCurrency($currency): self
    {
        $this->setCurrency($currency);

        return $this;
    }

    public function ofAmount(int $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function getCurrency(): ?ISO4217_Alpha_3
    {
        return $this->currency;
    }

    public function getHumanReadableAmount(...$params): ?float
    {
        if (!empty($params)) {
            $this->of(...$params);
        }

        if (!$this->hasCurrency() || !$this->hasAmount()) {
            return null;
        }
        return $this->isNonDecimal() ? $this->amount : ($this->amount / 100);
    }

    public function isNonDecimal($currency = null): bool
    {
        $this->setCurrency($currency);

        return !$this->hasCurrency() ||
            ($this->hasCurrency() && in_array(($this->currency), $this->NON_DECIMAL_CURRENCIES));

    }

    public function getNonDecimalCurrencies(): array
    {
        return $this->NON_DECIMAL_CURRENCIES;
    }
    private function hasCurrency(): bool
    {
        return isset($this->currency);
    }
    private function hasAmount(): bool
    {
        return isset($this->amount);
    }
}
