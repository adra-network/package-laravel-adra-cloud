<?php

namespace Adranetwork\AdraCloud\Facades;

use Illuminate\Support\Facades\Facade;

class CurrencyHelper extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'currency-helper';
    }
}
