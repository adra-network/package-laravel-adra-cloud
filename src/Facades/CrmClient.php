<?php

namespace Adranetwork\AdraCloud\Facades;

use Illuminate\Support\Facades\Facade;

class CrmClient extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'crm-client';
    }
}
