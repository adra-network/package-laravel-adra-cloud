<?php

namespace Adranetwork\AdraCloud\Facades;

use Illuminate\Support\Facades\Facade;

class EventSource extends Facade
{


    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'event-source';
    }
}
