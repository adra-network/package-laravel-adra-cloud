<?php

namespace Adranetwork\AdraCloud;

use Adranetwork\AdraCloud\Authorization\AuthorizationEnforcerContract;
use Adranetwork\AdraCloud\Authorization\Enforcers\OpaEnforcer;
use Adranetwork\AdraCloud\Clients\KeycloakClient;
use Adranetwork\AdraCloud\Clients\CrmClient;
use Adranetwork\AdraCloud\Clients\OpaClient;
use Adranetwork\AdraCloud\Contracts\Clients\AuthenticationClient;
use Adranetwork\AdraCloud\CurrencyHelper\CurrencyHelper;
use Adranetwork\AdraCloud\EventSource\Consumer\Triager;
use Adranetwork\AdraCloud\EventSource\Consumer\TriagerException;
use Adranetwork\AdraCloud\EventSource\Producer\KafkaProducer;
use Adranetwork\AdraCloud\EventSource\Producer\Producer;
use Adranetwork\AdraCloud\EventSource\Producer\RedisProducer;
use Adranetwork\AdraCloud\EventSource\EventSource;
use Illuminate\Support\ServiceProvider;

class AdraCloudServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang/', 'adra-cloud');

        $this->publishes([
            __DIR__.'/../config/adra-cloud.php' => config_path('adra-cloud.php'),
        ], 'adra-cloud-config');


        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/adra-cloud.php' => config_path('adra-cloud.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/adra-cloud.php', 'adra-cloud');


        // Register the main class to use with the facade
        $this->app->bind('currency-helper', function () {
            return new CurrencyHelper();
        });

        $this->app->bind(AuthorizationEnforcerContract::class, function ($app) {
            $class = config('adra-cloud.services.authorization.class_name');
            $class = $class ?: OpaEnforcer::class;
            return $app->make($class);
        });


        $this->app->singleton('opa-client',
            fn () =>  new OpaClient(
                baseUri: config('adra-cloud.services.authorization.url'),
                bearerToken: config('adra-cloud.services.authorization.token'),
                timeout: config('adra-cloud.services.authorization.timeout'),
                retryTimes:config('adra-cloud.services.authorization.retry'),
                retrySleep: config('adra-cloud.services.authorization.retry_sleep'))
        );

        $this->app->singleton('authorization-enforcer',
            fn ($app) =>$app->make(AuthorizationEnforcerContract::class)
        );
        $this->app->singleton(AuthenticationClient::class,
            fn () =>  new KeycloakClient(
                baseUri: config('adra-cloud.services.authentication.url'),
                realm: config('adra-cloud.services.authentication.realm'),
                openIdPath: config('adra-cloud.services.authentication.openid_configuration'),
            ));

        $this->app->singleton('authentication-client',
            fn ($app) =>$app->make(AuthenticationClient::class)
        );
        $this->app->singleton('crm-client',
            fn() => new CrmClient(
                baseUri: config('adra-cloud.services.crm.url'),
                timeout: 10,
                retryTimes: 1,
                retrySleep: 2
            ));

        $this->app->bind(Producer::class,
            fn ($app) =>config('adra-cloud.event.broker') === 'redis' ?
                new RedisProducer() : new KafkaProducer()
        );
        $this->app->bind(Triager::class, function() {
          $class = config('adra-cloud.event.triager');
            try {
                return new $class;
            }catch (\Error| \Exception){
               throw new TriagerException("could not load class: $class");
            }
        });

        $this->app->bind('event-source',
            fn ($app) =>
            new EventSource(
              $app->make(Producer::class)
            )
        );

    }
}
