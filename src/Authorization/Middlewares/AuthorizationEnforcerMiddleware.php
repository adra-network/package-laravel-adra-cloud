<?php

namespace Adranetwork\AdraCloud\Authorization\Middlewares;

use Adranetwork\AdraCloud\Facades\AuthorizationEnforcer;
use Illuminate\Http\Request;

class AuthorizationEnforcerMiddleware extends BaseAuthorizationEnforcerMiddleware
{

    public function allow(Request $request): bool
    {
        return AuthorizationEnforcer::allow();
    }
}
