<?php

namespace Adranetwork\AdraCloud\Authorization\Middlewares\Contracts;

use Closure;
use Illuminate\Http\Request;

interface EnforcerMiddlewareContract
{
    public function handle(Request $request, Closure $next): mixed;

    public function allow(Request $request): bool;
}
