<?php

namespace Adranetwork\AdraCloud\Authorization\Middlewares;

use Adranetwork\AdraCloud\Facades\AuthorizationEnforcer;
use Illuminate\Http\Request;

class TenancyAuthorizationEnforcerMiddleware extends BaseAuthorizationEnforcerMiddleware
{

    public function allow(Request $request): bool
    {
        return AuthorizationEnforcer::withExtraAttributes([
            'organization_id' => tenant('id')
        ])->allow();
    }
}
