<?php

namespace Adranetwork\AdraCloud\Authorization\Middlewares;

use Adranetwork\AdraCloud\Authorization\Exceptions\AuthorizationServiceException;
use Adranetwork\AdraCloud\Authorization\Exceptions\BadRequestException;
use Adranetwork\AdraCloud\Authorization\Middlewares\Contracts\EnforcerMiddlewareContract;
use Illuminate\Http\Request;
use Closure;

abstract class BaseAuthorizationEnforcerMiddleware implements EnforcerMiddlewareContract
{

    public function handle(Request $request, Closure $next): mixed
    {

        \Log::critical('Running TenancyAuthorizationEnforcerMiddleware');

        if (!config('adra-cloud.services.authorization.enabled')) {
            \Log::critical('Authorization is Disabled, skipping ');

            return $next($request);
        }
        try {
            if (!$this->allow($request)) {
                \Log::critical('AuthorizationEnforcer is false');
                throw new BadRequestException('Not Authorized', 403);
            }

            return $next($request);
        } catch (AuthorizationServiceException $exception) {
            return response()->json([
                'StatusCode' => 403,
                'message' => $exception->getMessage(),
            ], 403);
        } catch (\Exception $exception) {
            return response()->json([
                'StatusCode' => 403,
            ], 403);
        }
    }
}
