<?php

namespace Adranetwork\AdraCloud\Authorization\Enforcers;

use Adranetwork\AdraCloud\Authorization\AuthorizationEnforcerContract;
use Adranetwork\AdraCloud\Authorization\Exceptions\BadRequestException;
use Adranetwork\AdraCloud\Authorization\Exceptions\InvalidResponseContentException;
use Adranetwork\AdraCloud\Authorization\Exceptions\ServerErrorException;
use Adranetwork\AdraCloud\Authorization\Exceptions\ServiceNotReadyException;
use Adranetwork\AdraCloud\Facades\OpaClient;

class OpaEnforcer implements AuthorizationEnforcerContract
{

    protected array $payload;

    protected array $extraAttributes = [];

    public function allow(?string $bearerToken = null, ?string $method = null, ?string $path = null): bool
    {
        $this->setPayload($bearerToken, $method, $path);

        \Log::critical('OpaEnforcer Payload', $this->payload);
        $url = "/v1/data".config('adra-cloud.services.authorization.decision_path');

        \Log::critical("URL TO OPA IS : $url");
        $response = OpaClient::makeRequest()->post(
            url: "/v1/data".config('adra-cloud.services.authorization.decision_path'),
            data: $this->payload);

        if ($response->serverError()) {
            throw new ServerErrorException();
        }
        if ($response->clientError()) {
            throw new BadRequestException();
        }

        if (! isset($response->json()['result'])) {
            throw new ServiceNotReadyException();
        }

        if (! is_bool($response->json('result'))) {
            throw new InvalidResponseContentException();
        }

        return $response->json('result');
    }

    public function withExtraAttributes(array $extraAttributes = []): self
    {

        $this->extraAttributes = $extraAttributes;
        return $this;
    }

    private function setPayload($bearerToken, $method, $path)
    {

      $base = [
            'input' => [
                'authorization' => $bearerToken ?: request()->bearerToken() ?? '',
                'method' => strtoupper($method) ?: request()->getMethod(),
                'path' => $path ?: request()->path(),
                'service' => config('app.name'),
            ],
        ];
        if (! empty($this->extraAttributes)) {
            $base['input']['attributes'] = $this->extraAttributes;
        }
        $this->payload = $base;

    }

    public function getEnforcerName(): string
    {
        return get_class($this);
    }
}
