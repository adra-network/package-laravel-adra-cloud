<?php

namespace Adranetwork\AdraCloud\Authorization;

interface AuthorizationEnforcerContract
{

    public function allow(string $bearerToken = '', string $method = '', string $path = ''): bool;

    public function withExtraAttributes(array $extraAttributes = []): self;

    public function getEnforcerName(): string;
}
