<?php

namespace Adranetwork\AdraCloud\Contracts\Clients;

use Illuminate\Http\Client\PendingRequest;

interface Client
{
    public function makeRequest(): PendingRequest;
}
