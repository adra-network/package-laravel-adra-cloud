<?php

namespace Adranetwork\AdraCloud\Contracts\Clients;


use Illuminate\Http\Client\PendingRequest;

interface AuthenticationClient

{
    public function getUserIdByUsername(string $username): string;


    public function resetUserPassword(string $userId, ?string $password = null, array $options = []): string;


    public function makeAdminRequest(): PendingRequest;


    public function generateAccessToken($clientId = null, $clientSecret = null): string;






}
