<?php

namespace Adranetwork\AdraCloud\Clients;

use Adranetwork\AdraCloud\Contracts\Clients\Client;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class CrmClient implements Client
{
    public function __construct(
        public string $baseUri,
        public ?int $timeout,
        public ?int $retryTimes,
        public ?int $retrySleep)
    {}

    public function makeRequest($tenant = null): PendingRequest
    {

        $request = Http::baseUrl(
            url: $this->baseUri,
        )   ->asJson()
            ->when(!is_null($tenant),
                fn(PendingRequest $req) => $req->withHeaders([config('adra-cloud.tenancy.header_key_identifier') => $tenant])
            )
            ->withToken(\Adranetwork\AdraCloud\Facades\AuthenticationClient::generateAccessToken())
            ->timeout(
                seconds: $this->timeout,
            );

        if (! is_null($this->retryTimes) && ! is_null($this->retrySleep)) {
            $request->retry($this->retryTimes, $this->retrySleep);
        }
        return $request;
    }

}
