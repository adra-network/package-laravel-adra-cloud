<?php

namespace Adranetwork\AdraCloud\Clients;

use Adranetwork\AdraCloud\Contracts\Clients\Client;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class OpaClient implements Client
{
    public function __construct(
        public string $baseUri,
        protected ?string $bearerToken,
        public ?int $timeout,
        public ?int $retryTimes,
        public ?int $retrySleep)
    {}

    public function makeRequest(): PendingRequest
    {
        $request = Http::baseUrl(
            url: $this->baseUri,
        )   ->asJson()
            ->when(!empty($this->bearerToken), fn ($http) => $http->withToken($this->bearerToken))
            ->timeout(
                seconds: $this->timeout,
            );

        if (! is_null($this->retryTimes) && ! is_null($this->retrySleep)) {
            $request->retry($this->retryTimes, $this->retrySleep);
        }
        return $request;
    }

}
