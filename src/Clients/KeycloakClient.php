<?php

namespace Adranetwork\AdraCloud\Clients;


use Adranetwork\AdraCloud\Contracts\Clients\AuthenticationClient;
use Exception;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class KeycloakClient implements AuthenticationClient
{
    public string $tokenEndpoint;


    public function __construct(
        public string  $baseUri,
        public ?string $realm,
        public string  $openIdPath)
    {}

    protected function getOpenIdConfiguration()
    {
        Log::debug('Getting openIdConfiguration');
        Log::debug('Realm : '. $this->realm);
        Log::debug('openIdPath : '. $this->openIdPath);

        $request = $this->makeRequest();
        $url = sprintf('/realms/%s%s', $this->realm, $this->openIdPath);
        Log::debug('URL'. $url);

        $configuration = Cache::remember('oauth.openid_configuration.'. $this->realm, 30, fn() => $request
            ->get($url)
            ->json()
        );
        Log::debug('configuration', $configuration);
        Log::debug('tokenEndpoint' . $configuration['token_endpoint'] ?? 'undefined');

        $this->tokenEndpoint = $configuration['token_endpoint'];
    }

    public function makeRequest(): PendingRequest
    {
        return Http::baseUrl(
            url: $this->baseUri . config('adra-cloud.services.authentication.auth_prefix'),
        )->asForm()
            ->timeout(
                seconds: 10
            );
    }

    public function makeAdminRequest(): PendingRequest
    {

        $baseUrl = sprintf('%s%s/admin/realms/%s',
            $this->baseUri,
            config('adra-cloud.services.authentication.auth_prefix'),
            $this->realm
        );
        return Http::baseUrl(
            url: $baseUrl
        )->asJson()
            ->timeout(
                seconds: 10
            );
    }

    public function generateAccessToken($clientId = null, $clientSecret = null): string
    {
        $this->getOpenIdConfiguration();

        $clientId = $clientId ?: config('adra-cloud.services.authentication.client_id');
        $clientSecret = $clientSecret ?: config('adra-cloud.services.authentication.client_secret');
        Log::debug('clientId : ' . $clientId);
        Log::debug('clientSecret : ' . $clientSecret);

        return Cache::remember('oauth.access_token', 30, function () use ($clientId, $clientSecret) {

            $request = $this->makeRequest();
            $response = $request->post(
                url: $this->tokenEndpoint,
                data: [
                    'grant_type' => 'client_credentials',
                    'client_id' => $clientId,
                    'client_secret' => $clientSecret
                ]
            );
            if ($response->failed()) {
                throw new RequestException($response);
            }
            if (empty($response->json('access_token'))) {
                Log::critical('No access token found on oauth response');
                throw new \Exception($response);
            }
            return $response->json('access_token');
        }
        );
    }

    /**
     * @throws Exception
     */
    public function getUserIdByUsername(string $username): string
    {
        Log::info('Getting userId for username '. $username);

        $url = sprintf('/users?max=1&exact=true&username=%s', urlencode($username));
        $client = $this->makeAdminRequest()->withToken($this->generateAccessToken());
        $response = $client->get($url);

        if ($response->failed()) {
            throw new Exception('Something went wrong while fetching the user ID');
        }
        if (is_null($response->json('0.id'))) {
            throw new Exception('Could not find find the user ID for username' . $username);
        }


        return $response->json('0.id');
    }

    public function resetUserPassword(string $userId, ?string $password = null, array $options = []): string
    {

        $password = $password ?: Str::random();
        $client = $this->makeAdminRequest()->withToken($this->generateAccessToken());

        $response = $client
            ->asJson()
            ->put("/users/{$userId}/reset-password", [
                'type' => 'password',
                "value" => $password,
                "temporary" => $options['temporary'] ?? true
            ]);

        if ($response->status() !== 204) {
            throw new Exception('Could not reset password');
        }
        return $password;
    }
}
