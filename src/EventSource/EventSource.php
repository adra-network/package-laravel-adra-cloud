<?php

namespace Adranetwork\AdraCloud\EventSource;

use Adranetwork\AdraCloud\EventSource\Producer\Producer;

class EventSource
{

    public function __construct(
        protected Producer $producer,
    )
    {
    }

    public function producer(): Producer
    {
        return $this->producer;
    }


}
