<?php

namespace Adranetwork\AdraCloud\EventSource\Consumer;

use Adranetwork\AdraCloud\EventSource\StreamEvent;

interface Triager
{
    public function triage(StreamEvent $streamEvent): void;
}
