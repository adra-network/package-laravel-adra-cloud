<?php

namespace Adranetwork\AdraCloud\EventSource\Consumer;

use Adranetwork\AdraCloud\EventSource\StreamEvent;

class LogTriager implements Triager
{
    const FORMAT = '%s : %s';
    const RECEIVED_EVENT = 'RECEIVED EVENT';
    const TIMESTAMP = 'TIMESTAMP';
    const EVENT = 'EVENT';

    public function triage($streamEvent): void
    {
       \Log::info(sprintf(self::FORMAT, self::RECEIVED_EVENT, $streamEvent->getClassName()));
       \Log::info(sprintf(self::FORMAT, self::TIMESTAMP, $streamEvent->getTimestamp()));
       \Log::info(json_encode($streamEvent));
    }
}
