<?php

namespace Adranetwork\AdraCloud\EventSource\Consumer;

use Adranetwork\AdraCloud\EventSource\StreamEvent;
use Junges\Kafka\Contracts\KafkaConsumerMessage;
use Prwnr\Streamer\EventDispatcher\ReceivedMessage;

interface EventMessageHandler
{
    public function getStreamEvent(KafkaConsumerMessage | ReceivedMessage $message): StreamEvent;
}
