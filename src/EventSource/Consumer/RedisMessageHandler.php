<?php

namespace Adranetwork\AdraCloud\EventSource\Consumer;

use Adranetwork\AdraCloud\EventSource\StreamEvent;
use Adranetwork\AdraCloud\Facades\EventSource;
use Junges\Kafka\Contracts\KafkaConsumerMessage;
use Prwnr\Streamer\Contracts\MessageReceiver;
use Prwnr\Streamer\EventDispatcher\ReceivedMessage;

class RedisMessageHandler extends MessageHandler implements MessageReceiver
{

    public function handle(ReceivedMessage $message): void
    {
        $streamEvent = $this->getStreamEvent($message);
        $this->triager->triage($streamEvent);
    }

    public function getStreamEvent(ReceivedMessage|KafkaConsumerMessage $message): StreamEvent
    {
        $class = $message->get('className');
        $classData = json_decode($message->get('data'), true);
        return new $class($classData);
    }
}
