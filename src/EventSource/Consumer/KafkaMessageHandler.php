<?php

namespace Adranetwork\AdraCloud\EventSource\Consumer;


use Adranetwork\AdraCloud\EventSource\StreamEvent;
use Adranetwork\AdraCloud\Facades\EventSource;
use Junges\Kafka\Contracts\KafkaConsumerMessage;
use Prwnr\Streamer\EventDispatcher\ReceivedMessage;

class KafkaMessageHandler extends MessageHandler
{
    public function __execute(KafkaConsumerMessage $message)
    {
        $streamEvent = $this->getStreamEvent($message);

        $this->triager->triage($streamEvent);

    }

    public function getStreamEvent(ReceivedMessage|KafkaConsumerMessage $message): StreamEvent
    {
        $message = $message->getBody();
        $class = data_get($message, 'className');
        $classData = json_decode(data_get($message, 'data'), true);
        return new $class($classData);
    }
}
