<?php

namespace Adranetwork\AdraCloud\EventSource;



interface Event extends \JsonSerializable
{

    public function getEventName():string;

    public function getClassName(): string;

    public function getTimestamp(): string;

   public function dispatch();

}
