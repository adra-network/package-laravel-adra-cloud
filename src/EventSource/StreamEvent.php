<?php

namespace Adranetwork\AdraCloud\EventSource;


use Adranetwork\AdraCloud\Facades\EventSource;

abstract class StreamEvent implements Event
{
    private string $timestamp;

    public function __construct()
    {
        $this->timestamp = now()->toDateTimeString();
    }

    final public function getClassName(): string
    {
        return get_class($this);
    }

    final public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    final public function dispatch() {
        EventSource::producer()
            ->publishOn()
            ->withMessage($this)
            ->send();
    }
}
