<?php

namespace Adranetwork\AdraCloud\EventSource\Producer;





abstract class BaseProducer implements Producer
{
    protected string $stream;

    public function __construct()
    {
     $this->stream = config('adra-cloud.event.redis.stream');
    }
}
