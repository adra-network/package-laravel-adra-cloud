<?php

namespace Adranetwork\AdraCloud\EventSource\Producer;

use Adranetwork\AdraCloud\EventSource\Event;

interface Producer
{

    public function send();

    public function publishOn(string $stream = '');

    public function withMessage(Event $event);

}
