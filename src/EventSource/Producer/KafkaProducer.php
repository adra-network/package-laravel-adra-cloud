<?php

namespace Adranetwork\AdraCloud\EventSource\Producer;

use Adranetwork\AdraCloud\EventSource\Event;
use Adranetwork\AdraCloud\Facades\EventSource;
use Exception;
use Junges\Kafka\Config\Sasl;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;


class KafkaProducer extends BaseProducer
{

    protected Message $event;

    public function send()
    {
        if (!isset($this->event)) {
            throw new \Exception('Event could not be dispatched, missing event data');
        }
        $producer = Kafka::publishOn(
            $this->stream,
            config('adra-cloud.event.kafka.brokers')
        )
            ->withConfigOptions([
                'compression.codec' => config('adra-cloud.event.kafka.compression_type')
            ])
            ->withSasl(new Sasl(
                username: config('adra-cloud.event.kafka.username'),
                password: config('adra-cloud.event.kafka.password'),
                mechanisms:  config('adra-cloud.event.kafka.sasl_mechanisms'),
                securityProtocol:  config('adra-cloud.event.kafka.sasl_security_protocol'),
            ))
            ->withMessage($this->event);
            $producer->send();
    }

    public function publishOn(string $stream = ''): self
    {
        $this->stream = $stream ?: config('adra-cloud.event.kafka.topic');
       return $this;
    }

    public function withMessage(Event $event): self
    {
        $this->event = $this->makeEvent($event);

        return $this;
    }

    protected function makeEvent(Event $event): Message
    {

        return new Message(
            body: [
                'className' => $event->getClassName(),
                'eventName' => $event->getEventName(),
                'data' => json_encode($event)
            ]
        );
    }
}


