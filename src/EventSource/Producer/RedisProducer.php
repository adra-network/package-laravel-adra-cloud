<?php

namespace Adranetwork\AdraCloud\EventSource\Producer;

use Adranetwork\AdraCloud\EventSource\Event;
use Prwnr\Streamer\Contracts\Event as StreamerEvent;
use Prwnr\Streamer\Facades\Streamer;

class RedisProducer extends BaseProducer
{
    protected StreamerEvent $event;

    public function send()
    {
        if (!isset($this->event)) {
            throw new \Exception('Event could not be dispatched, missing event data');
        }
        Streamer::emit($this->event);
    }

    public function publishOn(string $stream = ''): self
    {
       $this->stream = $stream ?: config('adra-cloud.event.redis.stream');
       return $this;
    }

    public function withMessage(Event $event): self
    {
        $this->event = $this->makeEvent($event);
        return $this;
    }

    protected function makeEvent(Event $event): StreamerEvent
    {

        return new class($this->stream, $event) implements StreamerEvent {
            public function __construct(
                protected string $name,
                protected Event $event,
            )
            {}

            public function name(): string
            {
                return $this->name;
            }

            public function type(): string
            {
                return 'event';
            }

            public function payload(): array
            {
                return [
                    'className' => $this->event->getClassName(),
                    'eventName' => $this->event->getEventName(),
                    'data' => json_encode($this->event)
                ];
            }
        };
    }
}
